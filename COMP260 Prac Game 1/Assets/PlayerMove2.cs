﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove2 : MonoBehaviour
{

    public float maxSpeed = 5.0f; // in metres per second
    public float acceleration = 10.0f; // in metres/second/second
    public float brake = 5.0f; // in metres/second/second
    public float turnSpeed = 30.0f; // in degrees/second    
    private float speed = 0.0f; // in metres/second
    public string controlX;
    public string controlY;

    void Update()
    {
        // the horizontal axis controls the turn
        float turn = Input.GetAxis(controlX);
        // turn the car
        transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);

        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis(controlY);
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
                speed = Mathf.Max(0,speed);
            }
            else if(speed < 0)
            {
            
                speed = speed + brake * Time.deltaTime;
                speed = Mathf.Min(0, speed);
            }
        }
        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
    }
}