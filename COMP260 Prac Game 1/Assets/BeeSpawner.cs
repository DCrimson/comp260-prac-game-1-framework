﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{
    public BeeMove2 beePrefab;
    public PlayerMove player;
    public int nBees = 50;
    public Rect spawnRect;
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    private float speed;
    private float turnSpeed;
    private Transform target;
    private Vector2 heading;    public float BeePeriod;    public float minBeePeriod = 1f;    public float maxBeePeriod = 5f;
    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // BUG! the line below doesn’t work
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }

    void Start()
    {
        // create bees
        for (int i = 0; i < nBees; i++)
        {
            SpawnBee();
        }
    }

    void SpawnBee()
    {
        // instantiate a bee
        BeeMove2 bee = Instantiate(beePrefab);
        // set the target
        // attach to this object in the hierarchy
        bee.transform.parent = transform;
        // give the bee a name and number
        bee.gameObject.name = "Bee ";

        // find a player object to be the target by type
        // Note: this is not standard Unity syntax
        PlayerMove player = FindObjectOfType<PlayerMove>();
        bee.target = player.transform;

        // move the bee to a random position within
        // the spawn rectangle
        float x = spawnRect.xMin +
        Random.value * spawnRect.width;
        float y = spawnRect.yMin +
        Random.value * spawnRect.height;
        bee.transform.position = new Vector2(x, y);

        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
        Random.value);
    }


    // Update is called once per frame
    void Update()
    {
        BeePeriod = BeePeriod - Time.deltaTime;
        if (BeePeriod < 0f)
        {
            SpawnBee();
            BeePeriod = Mathf.Lerp(minBeePeriod, maxBeePeriod, Random.value);
        }
    }
    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

}
