﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour{
    // outer class definition omitted
    public Vector2 velocity; // in metres per second
    public float maxSpeed = 5.0f;

    public string controlSchemeX;
    public string controlSchemeY;

    private BeeSpawner beeSpawner;
    public float destroyRadius = 1.0f;

    void Start()
    {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }

    void Update()
    {
        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis(controlSchemeX);
        direction.y = Input.GetAxis(controlSchemeY);
        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;
        // move the object
        transform.Translate(velocity * Time.deltaTime);



        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
            transform.position, destroyRadius);
        }
    }
}
